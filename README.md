
Este é um microprojeto que mostra a utilização de rotas amigáveis em PHP sem o uso framework.

O arquivo index.php funciona como o proxyController. As rotas são definidas em config/routes.php

Os dados da aplicação estão na pasta app. 

Os controllers dessa aplicação tem que implementar a interface InterfaceRequest

Altere o namespace MyApp para o nome da sua aplicação. Faça isso também no arquivo composer.json

Após alterar o composer.json execute o comando: composer dumpautoload

Para colocar a aplicação no ar (em desenvolvimento), acesse, no terminal, a localização do projeto e execute

php -S localhost:8080 -f public

Assim, o servidor ficará no ar e apenas a pasta public poderá ser acessada. 

O composer está sendo usado para aplicar a PSR-4 com o seu autoload.