<?php

require_once(__DIR__ . '/../vendor/autoload.php');

use MyApp\Controller\InterfaceRequest;

$uri = $_SERVER['PATH_INFO'];
$routes = require __DIR__ . '/../config/routes.php';

if (!array_key_exists($uri, $routes)) { //if route exist
    http_response_code(404);
    exit();
}

$controlClass = $routes[$uri];
$controller = new $controlClass();
$controller->runRequest();