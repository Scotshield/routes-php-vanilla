<?php namespace MyApp\Controller;

interface InterfaceRequest
{
    public function runRequest():void;
}